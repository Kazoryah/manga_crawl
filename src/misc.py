class Language():
    FRENCH = "fr"
    ENGLISH = "en"

class Attribute():
    BASE_HTML = "base_html"
    CHAPTERS = "chapters"
    DONE = "done"
    FINISHED = "finished"
    FETCH_STATUS = "fetch_status"
    IN_PROGRESS = "in_progess"
    LANGUAGE = "language"
    LAST_UPDATE = "last_update"
    NAME = "name"
    PAGES = "pages"
    SOURCE_URL = "source_url"
    STATUS = "status"
    TO_DO = "to_do"
    URL = "url"

    NB_CHAPTERS = "nb_chapters"
    NB_PAGES = "nb_pages"

    CHAPTER_NB = "chapter_nb"
    PAGE_NB = "page_nb"
