class Logger():
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKCYAN = '\033[96m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'

    def parsing_info(self, name, number, total):
        print("Getting links for " + self.OKCYAN + name + self.ENDC + " (" + str(number) + "/" + str(total) + ")")
