from .misc import *
from .manga import Manga

class Mangas():
    KIMETSU_NO_YAIBA = "kimetsu_no_yaiba"


def get_links_kimetsu_no_yaiba(content):
    imgs = content.find(id='all')

    links = []
    for img in imgs.find_all('img'):
        links.append(img.get('data-src'))

    return links


mangas = [
            Manga(name=Mangas.KIMETSU_NO_YAIBA,
                  base_html="https://wwv.scan-1.com/kimetsu-no-yaiba/chapitre-",
                  language=Language.FRENCH,
                  nb_chapters=206,
                  parsing_function=get_links_kimetsu_no_yaiba,
                  status=Attribute.FINISHED)
         ]
