from .misc import *
import re
import datetime

def serialize_chapter(chapter_nb, links):
    pages = []
    for (page_nb, link) in enumerate(links):
        pages.append({ Attribute.PAGE_NB: page_nb + 1,
                       Attribute.URL: link.strip() })

    return { Attribute.CHAPTER_NB: chapter_nb,
             Attribute.NB_PAGES: len(links),
             Attribute.PAGES: pages }

class Manga():
    def __init__(self, name, base_html, language, nb_chapters, parsing_function,
                 status):
        self.name = name
        self.base_html = base_html
        self.language = language
        self.nb_chapters = nb_chapters
        self.parsing_function = parsing_function
        self.status = status

        self.chapters = []
        self.last_update = None
        self.fetch_status = Attribute.TO_DO

    def get_source_url(self):
        split = self.base_html.split('/')
        index = len(split[0] + split[2]) + 3
        return self.base_html[:index]

    def serialize(self):
        return {
                 Attribute.NAME: self.name,
                 Attribute.LANGUAGE: self.language,
                 Attribute.NB_CHAPTERS: self.nb_chapters,
                 Attribute.STATUS: self.status,
                 Attribute.FETCH_STATUS: self.fetch_status,
                 Attribute.LAST_UPDATE: self.last_update,
                 Attribute.SOURCE_URL: self.get_source_url(),
                 Attribute.CHAPTERS: self.chapters
               }

    def add_chapter(self, chapter):
        self.chapters.append(chapter)

    def pretty_name(self):
        name = self.name.split("_")
        camel_case = ''.join(x.title() for x in name)
        return re.sub("([a-z])([A-Z])","\g<1> \g<2>", camel_case)

    def done(self):
        self.fetch_status = Attribute.DONE
        self.last_update = datetime.datetime.now().isoformat()
