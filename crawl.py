from bs4 import BeautifulSoup
import concurrent.futures
import progressbar as pb
import requests
import time
import json
import os

from src import *

logger = Logger()

nb_workers = 15

def fetch(base_html, chapter_nb, parsing_function):
    page = requests.get(base_html + str(chapter_nb))
    content = BeautifulSoup(page.text, 'html.parser')

    links = parsing_function(content)
    return serialize_chapter(chapter_nb, links)

def get_links(manga):
    start_time = time.time()

    with concurrent.futures.ThreadPoolExecutor(max_workers=nb_workers) as executor:
        futures = []
        for chapter_nb in range(1, manga.nb_chapters + 1):
            futures.append(executor.submit(fetch, manga.base_html, chapter_nb,
                                           manga.parsing_function))

        count = 0
        with pb.ProgressBar(max_value=manga.nb_chapters) as bar:
            for future in concurrent.futures.as_completed(futures):
                manga.add_chapter(future.result())

                bar.update(count)
                count += 1


if __name__ == "__main__":
    res = []
    total = len(mangas)
    for (i, manga) in enumerate(mangas):
        logger.parsing_info(manga.pretty_name(), i + 1, total)
        get_links(manga)
        manga.done()
        res.append(manga.serialize())

    if not os.path.exists("data"):
        os.mkdir("data")

    with open("data/mangas.json", "w") as res_file:
        res_file.write(json.dumps(res))
